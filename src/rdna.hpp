#include <string>
#include <random>
#include <iostream>

using std::string;
using namespace std;

string randDNA(int seed, string bases, int n)
{
	int min = 0;
	int max = bases.size()-1;
	
	string result = "";
	
	std::mt19937 eng1(seed);
	std::uniform_int_distribution<int> unifrm(min,max);
	
	

	for(int i = 0; i < n; i++)
	{
		int r = unifrm(eng1);
		
		result = result + bases[r];
	}
	
	return result;
}
